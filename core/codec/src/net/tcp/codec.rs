extern crate frame;
use crate::net;
pub type TcpCodec = net::inner::codec::NetCodec;
